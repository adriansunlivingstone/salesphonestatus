﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace SalesPhoneStatus.Models
{
    public class Records
    {
        public string name { get; set; }
        public string phone { get; set; }
        public string team { get; set; }
        public string CurrentPhoneStatus { get; set; }
        public DateTime startTime { get; set; }
        //public DateTime? endTime { get; set; }

        public int TotalTime { get; set; }
    }
}
