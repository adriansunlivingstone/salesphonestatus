﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Livingstone.DBLib;

namespace SalesPhoneStatus.Models
{
    public class RetrieveTable
    {
        private static readonly IDBHandler dBHandler = new SqlServerDB();
        public static List<Records> resultList = new List<Records>();

        public static List<Records> RetrieveData()
        {
            resultList.Clear();
            List<List<object>> recList = new List<List<object>>();
            string sql = @"exec spSalesCurrentPhoneStatus";
            List<string> header = new List<string> { "name", "phoneNo", "team", "CurrentPhoneStatus", "startTime", "TotalTime" };
            dBHandler.getDataList(header, recList, null, sql, SqlServerDB.Servers.netcrmau);
            foreach (var line in recList)
            {
                Records re = new Records();
                re.name = line[0].ToString();
                re.phone = line[1].ToString();
                re.team = line[2].ToString();
                re.CurrentPhoneStatus = line[3].ToString();
                re.startTime = Convert.ToDateTime(line[4]);
                re.TotalTime = Convert.ToInt32(line[5]);
                resultList.Add(re);
            }

            return resultList;
        }

    }
}
